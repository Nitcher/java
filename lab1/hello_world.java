package first_try;

import java.util.Scanner;

public class hello_world {
	public static void main(String[] args) {
		try (Scanner scanner = new Scanner(System.in)) {
			System.out.print("Hello " + scanner.nextLine());
		}
	}
}
