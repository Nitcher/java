package first_try;

import java.util.Scanner;

public class square {
	public static void main(String[] args) {
		try (Scanner scanner = new Scanner(System.in)) {
			System.out.print("Введите длину ребра: ");
			int a = scanner.nextInt();
			double V = (5 * (3 + Math.sqrt(5)) * (Math.pow(a, 3))/12);
			System.out.print("Объем икосаэдра равен " + V);
		}
	}
}
