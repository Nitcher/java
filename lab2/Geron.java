import java.util.Scanner;

public class Geron {
    public static void main(String[] args) {
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.println("Введите число: ");
            double number = scanner.nextDouble();
            double x = number;
            System.out.println("Введите кол-во повторений");
            int repetition = scanner.nextInt();
            while (repetition > 0) {
                x = (0.5 * (x + (number / x)));
                repetition--;
            }
            System.out.println(x);
        }
    }
}