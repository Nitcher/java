import java.util.Scanner;

public class MaxValue {
    public static void main(String[] args) {
        try (Scanner scanner = new Scanner(System.in)) {
            String numbers = scanner.nextLine();
            String[] mas = numbers.split(" ");
            int masInt[] = new int[mas.length];
            for (int i = 0; i < mas.length; i++) {
                masInt[i] = Integer.parseInt(mas[i]);
            }
            int max = Integer.MIN_VALUE;
            int counter = 0;
            for (int i = 0; i < masInt.length; i++) {
                if (masInt[i] > max) {
                    max = masInt[i];
                    counter = 1;
                } else if (masInt[i] == max) {
                    counter++;
                }
            }
            System.out.println("Количество чисел, равных максимальному: ");
            System.out.println(counter);
        }
    }
}
