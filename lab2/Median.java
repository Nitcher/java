import java.util.Arrays;
import java.util.Scanner;

public class Median {
    public static void main(String[] args) {
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.println("Введите числа для сортировки: ");
            String numbers = scanner.nextLine();
            String[] mas = numbers.split(" ");
            int masInt[] = new int[mas.length];
            for (int i = 0; i < mas.length; i++) {
                masInt[i] = Integer.parseInt(mas[i]);
            }
            Arrays.sort(masInt);
            double median;
            if (masInt.length % 2 == 0)
                median = ((double) masInt[masInt.length / 2] + (double) masInt[masInt.length / 2 - 1]) / 2;
            else
                median = (double) masInt[masInt.length / 2];
            System.out.println(median);
        }
    }
}