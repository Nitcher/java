import java.util.Scanner;

public class MergeSort {
    public static void mergeSort(int[] a, int n) {
        if (n < 2) {
            return;
        }
        int mid = n / 2;
        int[] l = new int[mid];
        int[] r = new int[n - mid];

        for (int i = 0; i < mid; i++) {
            l[i] = a[i];
        }
        for (int i = mid; i < n; i++) {
            r[i - mid] = a[i];
        }
        mergeSort(l, mid);
        mergeSort(r, n - mid);

        merge(a, l, r, mid, n - mid);
    }

    public static void merge(
            int[] a, int[] l, int[] r, int left, int right) {

        int i = 0, j = 0, k = 0;
        while (i < left && j < right) {
            if (l[i] <= r[j]) {
                a[k++] = l[i++];
            } else {
                a[k++] = r[j++];
            }
        }
        while (i < left) {
            a[k++] = l[i++];
        }
        while (j < right) {
            a[k++] = r[j++];
        }
    }

    public static void main(String[] args) {
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.println("Введите числа для сортировки: ");
            String numbers = scanner.nextLine();
            String[] mas = numbers.split(" ");
            int masInt[] = new int[mas.length];
            for (int i = 0; i < mas.length; i++) {
                masInt[i] = Integer.parseInt(mas[i]);
            }
            mergeSort(masInt, masInt.length);
            System.out.println("Отсортированный массив: ");
            for (int i = 0; i < masInt.length; i++)
                System.out.print(masInt[i] + " ");
        }
    }
}