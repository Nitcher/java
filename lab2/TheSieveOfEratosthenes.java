import java.util.Arrays;
import java.util.Scanner;

public class TheSieveOfEratosthenes {
    public static void main(String[] args) {
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.println("Введите число: ");
            int number = scanner.nextInt();
            boolean[] sieve = new boolean[number];
            Arrays.fill(sieve, true);
            sieve[0] = false;
            sieve[1] = false;

            for (int i = 2; i * i < number; i++) {
                if (sieve[i]) {
                    for (int k = i * i; k < number; k += i) {
                        sieve[k] = false;
                    }
                }
            }
            System.out.println("Найденные простые числа: ");
            for (int i = 2; i < number; i++) {
                if (sieve[i]) {
                    System.out.println(i + " ");
                }
            }
            System.out.println();
        }
    }
}