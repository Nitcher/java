package lab3;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

public class DataFromTheSensor {
    public static void main(String[] args) {
        try (Scanner input = new Scanner(System.in)) {
            System.out.print("Введите показания датчиков: ");
            String read = input.nextLine();
            System.out.print("Введите 1, если хотите сортировать по id и 2, если по температуре ");
            int mode = input.nextInt();
            Integer[][] result = outputFromSensors(read);
            if (mode == 1) {
                Arrays.sort(result, Comparator.comparingDouble(o -> o[0]));
                for (int i = 0; i < result.length; i++)
                    System.out.println(result[i][0] + " " + result[i][1]);
            } else if (mode == 2) {
                Arrays.sort(result, Comparator.comparingDouble(o -> o[1]));
                for (int i = 0; i < result.length; i++)
                    System.out.println(result[i][0] + " " + result[i][1]);
            } else {
                System.out.println("Введите 1 или 2");
            }
        }

    }

    public static Integer[][] outputFromSensors(String read) {
        String[] text = read.split("@");
        Integer[][] output = new Integer[text.length][2];
        for (int i = 0; i < text.length; i++) {
            output[i][0] = Integer.parseInt(text[i].substring(0, 2));
            output[i][1] = Integer.parseInt(text[i].substring(2, text[i].length()));
        }
        return output;
    }
}
