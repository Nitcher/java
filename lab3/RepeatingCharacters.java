package lab3;

import java.util.Scanner;

public class RepeatingCharacters {
    public static void main(String args[]) {
        try (Scanner scanner = new Scanner(System.in, "cp866")) {
            System.out.println("Введите слово: ");
            String str = scanner.nextLine();
            str = str.replaceAll("(.)\\1{2,}", "$1");
            System.out.println(str);
        }
    }
}