package lab3;

import java.util.Scanner;

public class Substrings {

    private static int[] prefix(char[] str) {
        int len = str.length;
        int[] pi = new int[len];
        for (int i = 1; i < len; i++) {
            int j = pi[i - 1];
            while (j > 0 && str[i] != str[j])
                j = pi[j - 1];
            if (str[i] == str[j])
                j++;
            pi[i] = j;
        }
        return pi;
    }

    private static int searchMax(String substr, String str) {
        String full = substr + "#" + str;
        int[] prefix = prefix(full.toCharArray());
        int max = Integer.MIN_VALUE;
        int counter = 0;
        for (int i = 0; i < prefix.length; i++) {
            if (prefix[i] > max) {
                max = prefix[i];
                counter = 1;
            } else if (prefix[i] == max) {
                counter++;
            }
        }
        return counter;
    }

    public static void main(String args[]) {
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.println("Введите строки через пробел: ");
            String str = scanner.nextLine();
            System.out.println("Введите подстроку: ");
            String substr = scanner.nextLine();
            System.out.println(searchMax(substr, str));
        }
    }
}
