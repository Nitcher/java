package lab4;

public class MyLittleException extends Exception {
    public MyLittleException(Throwable cause) {
        super(cause);
    }

    public MyLittleException(String message) {
        super(message);
    }

    public MyLittleException(String message, Throwable cause) {
        super(message, cause);
    }

    public MyLittleException() {
        super();
    }

    @Override
    public String toString() {
        return "MyLittleException: " + this.getMessage();
    }
}