package lab4;

import java.io.*;
import java.util.Scanner;

public class ReadingFile {
    public static void main(String[] args) throws IOException {
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.println("Введите, где расположен файл: ");
            String file = scanner.nextLine();
            System.out.println(read(file));
        }
    }

    private static String read(String file) throws IOException {
        String result = null;
        try (DataInputStream reader = new DataInputStream(new FileInputStream(file))) {
            int nBytesToRead = reader.available();
            if (nBytesToRead > 0) {
                byte[] bytes = new byte[nBytesToRead];
                reader.read(bytes);
                result = new String(bytes);
            }
        }
        return result;
    }
}