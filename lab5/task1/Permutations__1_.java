package lab5;

import java.util.ArrayList;
import java.util.Scanner;

@SuppressWarnings("unchecked")

public class Permutations {
	static ArrayList<ArrayList<Integer>> resultList = new ArrayList<ArrayList<Integer>> ();
	static ArrayList<Integer> arrayList = new ArrayList<Integer>();
    static ArrayList<Integer> temp = new ArrayList<Integer>();

	public static void main(String args[]) {
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.print("Введите числа через пробел: ");
            String[] input = scanner.nextLine().split(" ");
			for (int i = 0; i < input.length; i++) arrayList.add(Integer.parseInt(input[i]));
            perms(0, arrayList.size()-1);
			System.out.println(resultList);
    }
    }

    public static void swap(int a, int b) {
		Integer swap = arrayList.get(a);
		arrayList.set(a, arrayList.get(b));
		arrayList.set(b, swap);
	}
    
	public static void perms(int a, int b) {
		if (a > b) {
            temp = (ArrayList<Integer>)arrayList.clone();
			resultList.add(temp);
		} else {
			for (int i = a; i <= b; i++) {
				swap(a, i);
				perms(a + 1, b);
				swap(a, i);
			}
		}
	}
}