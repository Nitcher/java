package lab5;

import java.util.ArrayList;
import java.util.Scanner;

public class Profit {
    public static int maxProfit(ArrayList<Integer> prices) {
        int len = prices.size();
        int answer = 0;
        for (int i = 0; i < len; i++) {
            for (int j = i + 1; j < len; j++) {
                answer = Math.max(answer, prices.get(j) - prices.get(i));
            }
        }
        return answer;
    }

    public static void main(String[] args) {
        ArrayList<Integer> prices = new ArrayList<>();
        System.out.println("Введите стоимость акций через запятую:");
        try (Scanner scanner = new Scanner(System.in)) {
            String temp = scanner.nextLine();
            String tempArr[] = temp.split(",");
            for (int i = 0; i < tempArr.length; i++) {
                prices.add(Integer.valueOf(tempArr[i]));
            }
            System.out.println(maxProfit(prices));
        }
    }
}