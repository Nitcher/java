package lab5;

import java.io.*;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

public class Customers {
    public static void main(String[] args) {
        Map<String, Map<String, Integer>> outputMap = new TreeMap<>();
        try {
            File file = new File("test.txt");
            FileReader fileReader = new FileReader(file);
            BufferedReader buffReader = new BufferedReader(fileReader);
            String inputString = buffReader.readLine();
            while (inputString != null) {
                mapHandler(outputMap, inputString);
                inputString = buffReader.readLine();
            }
            buffReader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ArrayList<String> mapKeys = new ArrayList<>(outputMap.keySet());
        for (int i = 0; i < mapKeys.size(); i++) {
            System.out.println(mapKeys.get(i) + ":");
            outputMap.get(mapKeys.get(i)).forEach((p, q) -> System.out.println((p + " - " + q)));
        }
    }

    static void mapHandler(Map<String, Map<String, Integer>> outputMap, String fileLine) {
        String[] splitedString = fileLine.split(" ");
        Map<String, Integer> insertedMap;
        if(outputMap.containsKey(splitedString[0])){
            insertedMap = outputMap.get(splitedString[0]);

            if(insertedMap.containsKey(splitedString[1])){
                int thingsQuantity = insertedMap.get(splitedString[1]);
                insertedMap.put(splitedString[1], thingsQuantity + Integer.valueOf(splitedString[2]));
            } else {
                insertedMap.put(splitedString[1], Integer.valueOf(splitedString[2]));
            }
        } else {
            insertedMap = new TreeMap<>();
            insertedMap.put(splitedString[1], Integer.valueOf(splitedString[2]));
            outputMap.put(splitedString[0], insertedMap);
        }
    }
}