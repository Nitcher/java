package lab5;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.HashSet;

public class Duplicates {
    public static void main(String[] args){
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.print("Введите элементы массива через запятую: ");
            String elems = scanner.nextLine();
            String[] stringArray = elems.split(",");
            ArrayList<Integer> elemsList = new ArrayList<Integer>();
            for (int i = 0; i < stringArray.length; i++) {
                elemsList.add(Integer.valueOf(stringArray[i]));
            }
            HashSet<Integer> elemsSet = new HashSet<Integer>(elemsList);
            System.out.print("Результат: " + elemsSet);
        }
    }
}